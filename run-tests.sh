#!/bin/sh

PATH=/home/postgres/parallel/pg-master/bin:$PATH
WARMUP=900
DURATION=$((7200))
SCALE=$1
RATE=$2
DIR=`pwd`

./collect-stats.sh &
./collect-wait-events.sh &

for t in minimal replica logical; do

	name="$t-$SCALE"

	pg_ctl -D /mnt/data/pgdata2 -w -t 3600 stop

	mkdir $name;

	cp postgresql-minimal.conf /mnt/data/pgdata2/postgresql.conf

	pg_ctl -D /mnt/data/pgdata2 -l $name/pg.log -w start

	sleep 5

	dropdb --if-exists pgbench
	createdb pgbench

	pgbench -i -s $SCALE pgbench > pgbench.txt 2>&1

        cp postgresql-$t.conf /mnt/data/pgdata2/postgresql.conf

        pg_ctl -D /mnt/data/pgdata2 -l $name/pg.log -w restart

        sleep 5

	psql -c "select * from pg_settings" postgres > $name/setting.log

	pgbench -c 32 -T $WARMUP pgbench > $name/warmup.log 2>&1

	psql -c "checkpoint" postgres

	rm -Rf /mnt/raid/wal/*
	rm *.log

	sleep 1

	pgbench -c 32 -T $DURATION -l --aggregate-interval=1 pgbench > $name/pgbench.log 2>&1

	mv pgbench_log.* $name
	mv *.log $name

	cd /mnt/raid/wal

	ls -l > $DIR/$name/xlog.list 2>&1

	echo xlog `ls | head -n 1` `ls | tail -n 1`

	pg_xlogdump --stats `ls | head -n 1` `ls | tail -n 1` > $DIR/$name/xlogdump.stats.log 2>&1
	pg_xlogdump --stats=record `ls | head -n 1` `ls | tail -n 1` > $DIR/$name/xlogdump.records.log 2>&1

	cd $DIR

done

for t in minimal replica logical; do

	name="$t-$SCALE-throttled"

        pg_ctl -D /mnt/data/pgdata2 -w -t 3600 stop

        mkdir $name;

        cp postgresql-minimal.conf /mnt/data/pgdata2/postgresql.conf

        pg_ctl -D /mnt/data/pgdata2 -l $name/pg.log -w start

        sleep 5

        dropdb --if-exists pgbench
        createdb pgbench

        pgbench -i -s $SCALE pgbench > pgbench.txt 2>&1

        cp postgresql-$t.conf /mnt/data/pgdata2/postgresql.conf

        pg_ctl -D /mnt/data/pgdata2 -l $name/pg.log -w restart

        sleep 5

        psql -c "select * from pg_settings" postgres > $name/setting.log

	pgbench -c 32 -T $WARMUP pgbench > $name/warmup.log 2>&1

        psql -c "checkpoint" postgres

	rm -Rf /mnt/raid/wal/*
	rm *.log

	sleep 1

        pgbench -c 32 -T $DURATION -l --aggregate-interval=1 -R $RATE pgbench > $name/pgbench.log 2>&1

        mv pgbench_log.* $name
        mv *.log $name

        cd /mnt/raid/wal

        ls -l > $DIR/$name/xlog.list 2>&1

	echo xlog `ls | head -n 1` `ls | tail -n 1`

        pg_xlogdump --stats `ls | head -n 1` `ls | tail -n 1` > $DIR/$name/xlogdump.stats.log 2>&1
        pg_xlogdump --stats=record `ls | head -n 1` `ls | tail -n 1` > $DIR/$name/xlogdump.records.log 2>&1

        cd $DIR

done

killall collect-stats.sh
killall collect-wait-events.sh
